<div>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Settings for') }} <span class="text-red-500">{{ $address->getFullAddress() }}</span>
            </h2>
            <a href="{{ route('mailbox', ['addressId' => $address->id]) }}">
                <x-jet-secondary-button>{{ __('Return') }}</x-jet-secondary-button>
            </a>
        </div>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-jet-form-section submit="save">
                <x-slot name="title">
                    {{ __('Mailbox Settings') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Configure various settings for this mailbox') }}
                </x-slot>

                <x-slot name="form">
                    <!-- Empty trash after X days -->
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="empty_trash_after_days" value="{{ __('Automatically empty trash after') }}" />
                        <x-jet-input-error for="empty_trash_after_days" class="mt-2" />

                        <select wire:model.defer="emptyTrashAfterDays" name="empty_trash_after_days" id="empty_trash_after_days" class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5">
                            @foreach ($emptyTrashAfterDaysOptions as $numDays)
                                <option value="{{ $numDays }}">{{ $numDays }} {{ __('days') }}</option>
                            @endforeach
                        </select>
                    </div>
                </x-slot>

                <x-slot name="actions">
                    <x-jet-action-message class="mr-3" on="saved">
                        {{ __('Saved.') }}
                    </x-jet-action-message>

                    <x-jet-button wire:loading.attr="disabled">
                        {{ __('Save') }}
                    </x-jet-button>
                </x-slot>
            </x-jet-form-section>

            <x-jet-section-border />

            <x-jet-form-section submit="saveFolders">
                <x-slot name="title">
                    {{ __('Folders') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Remove Folders and move their contents into another folder') }}
                </x-slot>

                <x-slot name="form">
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="deleteFolderSlug" value="{{ __('Folder to remove') }}" />
                        <x-jet-input-error for="deleteFolderSlug" class="mt-2" />

                        <select wire:model.defer="deleteFolderSlug" name="deleteFolderSlug" id="deleteFolderSlug" class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5">
                            @foreach ($address->folders()->excludeSystem()->get() as $folder)
                                <option value="{{ $folder->slug }}" wire:key="delete_folder_{{ $folder->slug }}">{{ $folder->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="moveFolderSlug" value="{{ __('Folder to move mails into') }}" />
                        <x-jet-input-error for="moveFolderSlug" class="mt-2" />

                        <select wire:model.defer="moveFolderSlug" name="moveFolderSlug" id="moveFolderSlug" class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5">
                            @foreach ($address->folders()->movableTo()->get() as $folder)
                                <option value="{{ $folder->slug }}" wire:key="move_folder_{{ $folder->slug }}">{{ $folder->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </x-slot>

                <x-slot name="actions">
                    <x-jet-action-message class="mr-3" on="savedFolder">
                        {{ __('Saved.') }}
                    </x-jet-action-message>

                    <x-jet-button wire:loading.attr="disabled">
                        {{ __('Save') }}
                    </x-jet-button>
                </x-slot>
            </x-jet-form-section>

            <x-jet-section-border />

            <x-jet-action-section>
                <x-slot name="title">
                    {{ __('Delete Address') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Permanently delete this email address') }}
                </x-slot>

                <x-slot name="content">
                    <div>
                        {{ __('Delete this email address. All mails will also be deleted.') }}
                    </div>
                    <div class="mt-5">
                        <x-jet-danger-button wire:click="confirmAddressDeletion" wire:loading.attr="disabled">
                            {{ __('Delete') }}
                        </x-jet-danger-button>

                        <!-- Delete User Confirmation Modal -->
                        <x-jet-dialog-modal wire:model="confirmingAddressDeletion">
                            <x-slot name="title">
                                {{ __('Delete Address') }}
                            </x-slot>

                            <x-slot name="content">
                                {{ __('Are you sure you want to delete this email address?') }}
                            </x-slot>

                            <x-slot name="footer">
                                <x-jet-secondary-button wire:click="$toggle('confirmingAddressDeletion')" wire:loading.attr="disabled">
                                    {{ __('Nevermind') }}
                                </x-jet-secondary-button>

                                <x-jet-danger-button class="ml-2" wire:click="deleteAddress" wire:loading.attr="disabled">
                                    {{ __('Delete Address') }}
                                </x-jet-danger-button>
                            </x-slot>
                        </x-jet-dialog-modal>
                    </div>
                </x-slot>
            </x-jet-action-section>
        </div>
    </div>
</div>
