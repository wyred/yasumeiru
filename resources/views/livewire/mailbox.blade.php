<div>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ $address->getFullAddress() }}
            </h2>
            <a href="{{ route('mailbox.settings', ['addressId' => $address->id]) }}">
                <x-jet-secondary-button>{{ __('Settings') }}</x-jet-secondary-button>
            </a>
        </div>
    </x-slot>

    <div class="pt-6 max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="px-2 md:px-0 flex justify-between">
            <nav class="flex flex-wrap">
                @foreach ($folders as $folder)
                    <a
                            href="{{ route('mailbox', ['addressId' => $address->id, 'folderSlug' => $folder->slug]) }}"
                            wire:key="{{ $folder->slug }}"
                            class="{{ $loop->first ? '' : 'ml-4' }} px-3 py-2 font-medium text-sm leading-5 rounded-md focus:outline-none hover:bg-gray-200 {{ ($activeFolder === $folder->slug) ? 'text-gray-800 bg-gray-300 hover:bg-gray-300' : 'text-gray-600' }}"
                            {{ ($activeFolder === $folder->slug) ? 'aria-current="page"' : '' }}>
                        {{ $folder->name }}
                    </a>
                @endforeach
                <a
                        href="{{ route('mailbox', ['addressId' => $address->id]) }}"
                        wire:key="null"
                        class="{{ empty($folders) ? '' : 'ml-4' }} px-3 py-2 font-medium text-sm leading-5 rounded-md focus:outline-none hover:bg-gray-200 {{ empty($activeFolder) ? 'text-gray-800 bg-gray-300 hover:bg-gray-300' : 'text-gray-600' }}"
                        {{ empty($activeFolder) ? 'aria-current="page"' : '' }}>
                    {{ __('All Mail') }}
                </a>

                <div wire:click="$toggle('showAddFolderModal')" class="ml-4 px-3 py-2 font-medium text-sm leading-5 rounded-md focus:outline-none text-gray-600 focus:text-gray-800 focus:bg-gray-200 cursor-pointer hover:bg-gray-200">&plus;</div>

                <x-jet-dialog-modal wire:model="showAddFolderModal">
                    <x-slot name="title">
                        {{ __('Add Folder') }}
                    </x-slot>

                    <x-slot name="content">
                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="newFolderName" value="{{ __('Folder Name') }}" class="mt-5" />
                            <x-jet-input wire:model.defer="newFolderName" wire:keydown.enter="createFolder" name="newFolderName" id="newFolderName" class="w-full" />
                            <x-jet-input-error for="newFolderName" class="mt-2" />
                        </div>
                    </x-slot>

                    <x-slot name="footer">
                        <x-jet-secondary-button wire:click="$toggle('showAddFolderModal')" wire:loading.attr="disabled">
                            {{ __('Cancel') }}
                        </x-jet-secondary-button>

                        <x-jet-button class="ml-2" wire:click="createFolder" wire:loading.attr="disabled">
                            {{ __('Create Folder') }}
                        </x-jet-button>
                    </x-slot>
                </x-jet-dialog-modal>
            </nav>

            @if ($this->activeFolder === 'trash')
                <x-jet-danger-button wire:click="emptyTrash">{{ __('Empty Trash') }}</x-jet-danger-button>
            @else
                <x-jet-secondary-button wire:click="$toggle('showComposeModal')">{{ __('Compose') }}</x-jet-secondary-button>
                <x-jet-dialog-modal wire:model="showComposeModal">
                    <x-slot name="title">
                        {{ __('Compose Mail') }}
                    </x-slot>

                    <x-slot name="content">
                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="newEmailTo" value="{{ __('To') }}" class="mt-5" />
                            <x-jet-input wire:model.defer="newEmailTo" name="newEmailTo" id="newEmailTo" class="w-full" />
                            <x-jet-input-error for="newEmailTo" class="mt-2" />
                        </div>
                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="newEmailSubject" value="{{ __('Subject') }}" class="mt-5" />
                            <x-jet-input wire:model.defer="newEmailSubject" name="newEmailSubject" id="newEmailSubject" class="w-full" />
                            <x-jet-input-error for="newEmailSubject" class="mt-2" />
                        </div>
                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="newEmailMessage" value="{{ __('Message') }}" class="mt-5" />
                            <textarea wire:model.defer="newEmailMessage" name="newEmailMessage" id="newEmailMessage" class="w-full h-32 form-input rounded-md shadow-sm"></textarea>
                            <x-jet-input-error for="newEmailMessage" class="mt-2" />
                        </div>
                    </x-slot>

                    <x-slot name="footer">
                        <x-jet-secondary-button wire:click="$toggle('showComposeModal')" wire:loading.attr="disabled">
                            {{ __('Cancel') }}
                        </x-jet-secondary-button>

                        <x-jet-button class="ml-2" wire:click="sendEmail" wire:loading.attr="disabled">
                            {{ __('Send') }}
                        </x-jet-button>
                    </x-slot>
                </x-jet-dialog-modal>
            @endif
        </div>
    </div>

    <div wire:poll.5s class="pt-6 max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if ($mails->isEmpty())
            <div class="mt-5 bg-white w-auto px-3 py-2 rounded-md shadow text-center text-gray-300">No Mails</div>
        @else
            <div class="flex flex-col">
                <div class="-my-2 sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block sm:px-6 lg:px-8">
                        <div class="max-w-7xl shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            @foreach ($mails as $mail)
                                <a href="{{ route('mail', ['addressId' => $address, 'mailId' => $mail->id]) }}">
                                    <div class="grid grid-cols-8 gap-0 sm:gap-5 bg-white {{ $mail->read_at ? 'font-normal' : 'font-bold' }} {{ $loop->first ? '' : 'border-t border-gray-300' }}">
                                        <div class="col-span-8 sm:col-span-2 px-4 py-2 sm:py-4 flex items-center">
                                            <div class="hidden sm:block flex-shrink-0 h-10 w-10">
                                                <img class="h-10 w-10 rounded-full" src="{{ gravatar($mail->from_email) }}" alt="">
                                            </div>
                                            <div class="sm:ml-4">
                                                <div class="text-sm leading-5 text-black">
                                                    {{ $mail->from_name }}
                                                </div>
                                                <div class="hidden sm:block text-sm leading-5 text-gray-400 truncate">
                                                    {{ $mail->from_email }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-span-5 sm:col-span-5 px-4 py-0 sm:py-4 whitespace-no-wrap">
                                            <div class="text-sm leading-5 text-black">{{ $mail->subject }}</div>
                                            <div class="text-sm leading-5 text-gray-400 truncate">
                                                {{ $mail->content }}
                                            </div>
                                        </div>
                                        <div class="col-span-3 sm:col-span-1 p-4 text-right text-xs leading-5 font-medium text-gray-400">
                                            {{ user_datetime_format($mail->created_at) }}
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="mt-5 px-5 sm:px-0">
            {{ $mails->links() }}
        </div>
    </div>
</div>
