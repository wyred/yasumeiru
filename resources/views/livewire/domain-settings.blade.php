<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settings for') }} <span class="text-red-500">{{ $domain->name }}</span>
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-jet-form-section submit="save">
                <x-slot name="title">
                    {{ __('Domain Settings') }}
                </x-slot>

                <x-slot name="description">
                    &nbsp;
                </x-slot>

                <x-slot name="form">
                    <!-- Discord Notification URL -->
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="discord_notification_url" value="{{ __('Discord Notification URL') }}" />

                        <x-jet-input id="discord_notification_url"
                                     type="text"
                                     class="mt-1 block w-full"
                                     wire:model.defer="state.discord_notification_url"
                                     :disabled="! Gate::check('update', $domain)" />

                        <x-jet-input-error for="state.discord_notification_url" class="mt-2" />
                    </div>

                    <!-- Mail Service -->
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="mailservice" value="{{ __('Mail Service') }}" />
                        <x-jet-input-error for="mailservice" class="mt-2" />

                        <select wire:model.defer="state.mailservice" name="mailservice" id="mailservice" class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5">
                            @foreach ((new \App\Library\MailServices\MailServiceManager)->getAvailableServices() as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Mail Service API Key-->
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="mailservice_api_key" value="{{ __('Mail Service API Key') }}" />
                        <x-jet-input id="mailservice_api_key" type="text" class="mt-1 block w-full" wire:model.defer="state.mailservice_api_key" />
                        <x-jet-input-error for="mailservice_api_key" class="mt-2" />
                    </div>

                    <!-- CatchAll Mail -->
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="catch_all_address_id" value="{{ __('Catch-All Email') }}" />
                        <x-jet-input-error for="catch_all_address_id" class="mt-2" />

                        <select wire:model="state.catch_all_address_id.defer" name="catch_all_address_id" id="catch_all_address_id" class="mt-1 form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5" aria-describedby="catch_all_description">
                            <option value="" {{ $domain->catch_all_address_id === null ? 'selected' : '' }}>Discard</option>
                            @foreach ($domain->addresses as $address)
                                <option value="{{ $address->id }}">{{ $address->name }} - {{ $address->getFullAddress() }}</option>
                            @endforeach
                        </select>
                        <p class="mt-2 text-sm text-gray-500" id="catch_all_description">{{ __('Select an address to receive all emails that are sent to an email address that does not exist.') }}</p>
                    </div>
                </x-slot>

                <x-slot name="actions">
                    <x-jet-action-message class="mr-3" on="saved">
                        {{ __('Saved.') }}
                    </x-jet-action-message>

                    <x-jet-button wire:loading.attr="disabled" wire:target="photo">
                        {{ __('Save') }}
                    </x-jet-button>
                </x-slot>
            </x-jet-form-section>

        </div>
    </div>
</div>
