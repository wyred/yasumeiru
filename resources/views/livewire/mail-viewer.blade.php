<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $mail->subject }}
        </h2>
    </x-slot>

    <div wire:poll class="pt-5 pb-20">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="px-2 sm:px-0 flex flex-wrap justify-between">
                <div class="my-1 lg:my-0">
                    <a href="{{ $returnUrl }}" class="inline-flex items-center px-4 py-2 bg-white border border-gray-300 rounded-md font-semibold text-xs text-gray-700 uppercase tracking-widest shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150">{{ __('Return') }}</a>
                </div>

                @if ($mail->isInInbox())
                    <div class="my-1 lg:my-0">
                        <x-jet-secondary-button wire:click="archive()">{{ __('Archive') }}</x-jet-secondary-button>
                    </div>
                @endif

                @if (! $mail->isInOutbox())
                    <div class="my-1 lg:my-0 flex items-center">
                        <span class="flex-none">Folder</span>
                        <select wire:model.defer="destinationFolder" name="destinationFolder" id="destinationFolder" class="ml-2 flex-shrink w-full form-select block pl-3 pr-10 py-2 text-sm leading-5 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300">
                            @foreach ($mail->address->folders()->where('is_system', false)->get() as $folder)
                                <option value="{{ $folder->slug }}">{{ $folder->name }}</option>
                            @endforeach
                            <option value="inbox">{{ __('Inbox') }}</option>
                            <option value="null">{{ __('All Mail') }}</option>
                        </select>
                        <span class="flex-none ml-2">
                            <x-jet-secondary-button wire:click="moveToFolder">{{ __('Move') }}</x-jet-secondary-button>
                        </span>
                    </div>
                @endif

                <div class="my-1 lg:my-0">
                    @if ($mail->isBlocked())
                        <x-jet-secondary-button wire:click="unblock()">{{ __('Unblock Sender') }}</x-jet-secondary-button>
                    @else
                        <x-jet-danger-button wire:click="block()">{{ __('Block Sender') }}</x-jet-danger-button>
                    @endif

                    @if ($mail->trashed())
                        <x-jet-secondary-button wire:click="restore()">{{ __('Restore') }}</x-jet-secondary-button>
                    @else
                        <x-jet-danger-button wire:click="delete()">{{ __('Delete') }}</x-jet-danger-button>
                    @endif
                </div>
            </div>

            <x-mail wire:key="mail_{{ $mail->id }}" :mail="$mail" :parser="$parser"></x-mail>

            <div class="mt-5">
                <div class="uppercase font-bold text-gray-500">Reply to {{ $replyToName }}</div>
                <div>
                    <textarea wire:model.defer="reply" name="reply" id="reply" class="w-full h-64 p-5 rounded-md"></textarea>
                </div>
                <div class="flex justify-end items-center">
                    <div>
                        <x-jet-action-message class="mr-3" on="sent">
                            {{ __('Sent.') }}
                        </x-jet-action-message>
                    </div>
                    <x-jet-button wire:click="sendReply()">Send</x-jet-button>
                </div>
            </div>
        </div>
    </div>
</div>
