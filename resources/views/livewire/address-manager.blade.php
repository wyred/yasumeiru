<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Addresses') }} in <span class="text-red-500">{{ $domain->name }}</span>
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <p>Select an email address</p>
            <div wire:poll.10s class="mt-3 grid grid-cols-1 md:grid-cols-5 md:gap-5">
                @foreach ($addresses as $address)
                    <a wire:key="{{ $address->id }}" href="{{ route('mailbox', ['addressId' => $address->id, 'folderSlug' => 'inbox']) }}">
                        <div class="bg-white h-32 flex flex-col items-center sm:rounded-md justify-center shadow hover:scale-110 hover:shadow-lg transform duration-300">
                            <span class="text-2xl text-gray-500">{{ $address->email_lhs }}</span>
                            <span class="text-sm text-gray-400">{{ $address->getUnreadMailsCount() }} {{ __('unread') }}, {{ $address->getTotalMailsCount() }} {{ __('emails') }}</span>
                        </div>
                    </a>
                @endforeach

                <div wire:click="$toggle('showAddAddressModal')" wire:key="add_address" class="bg-white h-32 inline-flex items-center sm:rounded-md justify-center cursor-pointer shadow hover:scale-110 hover:shadow-lg transform duration-300">
                    <span class="text-4xl text-gray-300">&plus;</span>
                </div>

                <x-jet-dialog-modal wire:model="showAddAddressModal">
                    <x-slot name="title">
                        {{ __('Add Address') }}
                    </x-slot>

                    <x-slot name="content">
                        <div class="col-span-6 sm:col-span-4">
                            <x-jet-label for="emailName" value="{{ __('Display Name') }}" class="mt-1" />
                            <x-jet-input wire:model="emailName" name="emailName" class="w-full" />
                            <x-jet-input-error for="emailName" class="mt-2" />

                            <x-jet-label for="emailAddress" value="{{ __('Email Address') }}" class="mt-3" />
                            <div class="flex rounded-md shadow-sm">
                                <input
                                        wire:model.defer="emailAddress"
                                        id="emailAddress"
                                        class="form-input text-right flex-1 block w-full px-3 py-2 rounded-none rounded-l-md outline-none sm:text-sm sm:leading-5">
                                <span class="inline-flex items-center px-3 rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">
                                    {{ '@' . $domain->name }}
                                </span>
                            </div>

                            <x-jet-input-error for="emailAddress" class="mt-2" />
                        </div>
                    </x-slot>

                    <x-slot name="footer">
                        <x-jet-secondary-button wire:click="$toggle('showAddAddressModal')" wire:loading.attr="disabled">
                            {{ __('Cancel') }}
                        </x-jet-secondary-button>

                        <x-jet-button class="ml-2" wire:click="createAddress" wire:loading.attr="disabled">
                            {{ __('Create Address') }}
                        </x-jet-button>
                    </x-slot>
                </x-jet-dialog-modal>
            </div>
        </div>
    </div>
</div>
