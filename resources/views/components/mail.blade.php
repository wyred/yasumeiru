<div class="mt-5 bg-white rounded-md shadow overflow-hidden">
    <div class="flex justify-between p-3 border-b border-gray-100">
        <div class="flex items-center">
            <svg class="w-5 h-5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16 7C16 9.20914 14.2091 11 12 11C9.79086 11 8 9.20914 8 7C8 4.79086 9.79086 3 12 3C14.2091 3 16 4.79086 16 7Z" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M12 14C8.13401 14 5 17.134 5 21H19C19 17.134 15.866 14 12 14Z" stroke="#4A5568" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
            <span class="ml-2">{{ $mail->from_name }}</span>
            <span class="ml-5 text-sm text-red-500">{{ $mail->from_email }}</span>
        </div>
        <div class="items-center">
            <span class="ml-5 text-sm text-gray-400">{{ user_datetime_format($mail->sent_at) }}</span>
        </div>
    </div>
    <div class="p-5">
        @php
            $parser->setText($mail->raw);
            $html = $parser->getMessageBody('htmlEmbedded');
            if (empty($html)) {
                $html = $mail->content;
            }
        @endphp
        {!! $html !!}
    </div>
    <div class="p-3 border-t border-gray-100 flex items-center justify-between">
        <div>
            <x-reply-button wire:click="setReplyTo({{ $mail->id }}, '{{ $mail->from_name }}')" />
        </div>
        <div class="text-xs text-gray-400">
            @if ($mail->status === 'open')
                Opened on {{ $mail->read_at }}
            @else
                {{ $mail->status }}
            @endif
            &nbsp;
        </div>
    </div>
</div>

@foreach ($mail->replies as $reply)
    <div class="ml-5">
        <x-mail wire:key="mail_{{ $reply->id }}" :mail="$reply" :parser="$parser"></x-mail>
    </div>
@endforeach
