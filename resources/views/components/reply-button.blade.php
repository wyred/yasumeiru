<button {{ $attributes->merge(['type' => 'button', 'class' => 'inline-flex items-center px-2 py-1 bg-green-100 rounded-md font-semibold text-xs text-green-700 uppercase tracking-widest shadow-sm hover:text-green-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-green-800 active:bg-green-50 transition ease-in-out duration-150']) }}>
    Reply
</button>
