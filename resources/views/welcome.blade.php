<x-guest-layout>
    <div class="mx-auto w-max-content h-screen flex text-center items-center">
        <div class="flex flex-col">
            <h1 class="text-6xl text-red-500">yasumeiru</h1>
            <p class="font-thin text-2xl">self hosted email web app</p>
            <a href="{{ route('login') }}" class="mt-5 bg-red-500 hover:bg-red-600 text-white rounded-md p-2">Login</a>
        </div>
    </div>
</x-guest-layout>
