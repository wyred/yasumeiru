@component('mail::layout')
{{-- Header --}}
@slot('header')
&nbsp;
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
sent from {{ config('app.name') }}
@endcomponent
@endslot
@endcomponent
