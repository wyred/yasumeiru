<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([])->group(function () {
    Route::post('mailservice/{domain_identifier}/incoming', [\App\Library\MailServices\MailServiceController::class, 'incoming'])->name('api.mailservice.incoming');
    Route::post('mailservice/{domain_identifier}/events', [\App\Library\MailServices\MailServiceController::class, 'events'])->name('api.mailservice.events');
});
