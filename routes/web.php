<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function() {
    Route::get('/domain/settings', \App\Http\Livewire\DomainSettings::class)->name('domain.settings');

    Route::get('/addresses', \App\Http\Livewire\AddressManager::class)->name('addresses');
    Route::get('/addresses/{addressId}/settings', \App\Http\Livewire\MailboxSettings::class)->name('mailbox.settings');
    Route::get('/addresses/{addressId}/mailbox/{folderSlug?}', \App\Http\Livewire\Mailbox::class)->name('mailbox');
    Route::get('/addresses/{addressId}/mail/{mailId}', \App\Http\Livewire\MailViewer::class)->name('mail');
});
