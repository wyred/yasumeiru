<?php

namespace App\Console\Commands;

use App\Models\Address;
use Illuminate\Console\Command;

class EmptyTrash extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yasumeiru:empty_trash';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Empties trash based on age setting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Address::all()
            ->each(function (Address $address) {
                $address->mails()
                    ->onlyTrashed()
                    ->where('deleted_at', '<', now()->subDays($address->empty_trash_after_days))
                    ->forceDelete();
            });
    }
}
