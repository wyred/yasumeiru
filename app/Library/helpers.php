<?php

function gravatar($email): string
{
    $hash = md5(strtolower(trim($email)));

    return "https://www.gravatar.com/avatar/$hash?d=mp";
}

function clean_message_id($messageId = null)
{
    if (empty($messageId)) {
        return null;
    }

    $messageId = str_replace(['<', '>'], '', $messageId);
    $messageId = explode('@', $messageId, 3)[0];
    return explode('.', $messageId, 3)[0];
}

function is_carbon($item): bool
{
    if (! is_object($item)) {
        return false;
    }

    $class = get_class($item);

    return ($class === 'Carbon\Carbon' or $class === 'Illuminate\Support\Carbon');
}

function ensure_carbon($string = null): \Carbon\Carbon
{
    if (is_carbon($string)) {
        return clone $string;
    }

    if (empty($string)) {
        return now();
    }

    return \Carbon\Carbon::parse($string);
}

function user_datetime_format($date): string
{
    return ensure_carbon($date)
        ->setTimezone(auth()->user()->timezone)
        ->format('Y-m-d H:i:s');
}
