<?php

namespace App\Library;

use App\Models\Address;
use App\Models\BlockedEmail;

class Doorman
{
    /**
     * @var Address
     */
    public $address;

    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    public function hasBlocked($email)
    {
        return $this->address->blockedEmailAddresses()
            ->where('email', $email)
            ->exists();
    }

    public function blockEmail($email)
    {
        $blockedEmail = new BlockedEmail();
        $blockedEmail->email = $email;
        $blockedEmail->address()->associate($this->address);
        $blockedEmail->save();
    }

    public function unblockEmail($email)
    {
        $this->address->blockedEmailAddresses()
            ->where('email', $email)
            ->delete();
    }
}
