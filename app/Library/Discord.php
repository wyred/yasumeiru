<?php

namespace App\Library;

use Illuminate\Support\Facades\Http;

class Discord
{
    public $webhook;
    public $username = '';
    public $content = '';
    public $embeds = [];

    public function setWebhook($url)
    {
        $this->webhook = $url;
        return $this;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function embed($content)
    {
        $this->embeds[] = $content;
        return $this;
    }

    public function send()
    {
        Http::post($this->webhook, [
            'username' => $this->username,
            'content' => $this->content,
            'embeds' => $this->embeds,
        ]);
    }
}
