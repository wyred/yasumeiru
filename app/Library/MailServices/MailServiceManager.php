<?php

namespace App\Library\MailServices;

use App\Library\MailServices\Local\Local;
use App\Library\MailServices\Sendgrid\Sendgrid;
use App\Models\Mail;
use App\Models\Team;

class MailServiceManager
{
    public $services = [
        'local' => Local::class,
        'sendgrid' => Sendgrid::class,
    ];

    public function getAvailableServiceKeys(): array
    {
        return array_keys($this->services);
    }

    public function getAvailableServices(): array
    {
        $results = [];

        foreach ($this->services as $key => $class) {
            if (app()->environment('production') and $key === 'local') {
                continue;
            }

            $results[$key] = $class::getName();
        }

        return $results;
    }

    public function getMailservice(Team $domain): MailServiceContract
    {
        if (! isset($this->services[$domain->mailservice])) {
            return new Local($domain);
        }

        return (new $this->services[$domain->mailservice]($domain));
    }

    public function setup(Team $domain): void
    {
        if (empty($domain->mailservice_api_key)) {
            return;
        }

        $this->getMailservice($domain)->installWebhooks();
    }

    public function processIncoming(Team $domain, array $request): void
    {
        $this->getMailservice($domain)->processMail($request);
    }

    public function processEvent(Team $domain, array $request): void
    {
        $this->getMailservice($domain)->processEvent($request);
    }

    public function sendEmail(Mail $mail)
    {
        $this->getMailservice($mail->domain)->sendMail($mail);
    }
}
