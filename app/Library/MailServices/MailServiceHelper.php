<?php

namespace App\Library\MailServices;

class MailServiceHelper
{
    public static function emailBelongsToDomain($email, $domain)
    {
        return strtolower(explode('@', $email)[1]) === strtolower($domain);
    }

    public static function getEmailLocalName($email)
    {
        return strtolower(explode('@', $email)[0]);
    }
}
