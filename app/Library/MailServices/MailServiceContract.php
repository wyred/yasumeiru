<?php

namespace App\Library\MailServices;

use App\Models\Mail;
use App\Models\Team;

interface MailServiceContract
{
    public function __construct(Team $domain);

    public static function getName(): string;

    public function installWebhooks(): void;

    public function sendMail(Mail $mail): void;

    public function processMail(array $request): void;

    public function processEvent(array $request): void;
}
