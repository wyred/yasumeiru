<?php

namespace App\Library\MailServices\Sendgrid;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Setup
{
    public $apiKey;
    private $apiUrl = 'https://api.sendgrid.com/v3';

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function setupParseWebhook(array $params): self
    {
        $response = Http::withToken($this->apiKey)
            ->post($this->apiUrl . '/user/webhooks/parse/settings', $params);

        if ($response->failed()) {
            Log::error('MailService - Sendgrid - setupParseWebhook', $response->json());
            return $this;
        }

        Log::info('MailService - Sendgrid - setupParseWebhook', $response->json());
        return $this;
    }

    public function setupClicksWebhook(array $params): self
    {
        $response = Http::withToken($this->apiKey)
            ->post($this->apiUrl . '/user/webhooks/event/settings', $params);

        if ($response->failed()) {
            Log::error('MailService - Sendgrid - setupClicksWebhook', $response->json());
            return $this;
        }

        Log::info('MailService - Sendgrid - setupClicksWebhook', $response->json());
        return $this;
    }
}
