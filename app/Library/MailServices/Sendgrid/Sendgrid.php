<?php

namespace App\Library\MailServices\Sendgrid;

use App\Jobs\DiscordNotificationJob;
use App\Library\Doorman;
use App\Library\MailServices\MailServiceContract;
use App\Library\MailServices\MailServiceHelper;
use App\Mail\SimpleMail;
use App\Models\Mail;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use PhpMimeMailParser\Parser;

class Sendgrid implements MailServiceContract
{
    /**
     * @var Team
     */
    public $domain;
    private $apiUrl = 'https://api.sendgrid.com/v3';

    public function __construct(Team $domain)
    {
        $this->domain = $domain;
    }

    public static function getName(): string
    {
        return 'SendGrid';
    }

    public function installWebhooks(): void
    {
        (new Setup($this->domain->mailservice_api_key))
            ->setupParseWebhook([
                'hostname' => $this->domain->name,
                'url' => route('api.mailservice.incoming', ['domain_identifier' => $this->domain->identifier]),
                'spam_check' => true,
                'send_raw' => true,
            ])->setupClicksWebhook([
                'enabled' => true,
                'url' => route('api.mailservice.events', ['domain_identifier' => $this->domain->identifier]),
                'delivered' => true,
                'bounce' => true,
                'dropped' => true,
                'deferred' => true,
                'open' => true,
            ]);
    }

    public function sendMail(Mail $mail): void
    {
        $response = Http::withToken($this->domain->mailservice_api_key)
            ->post($this->apiUrl . '/mail/send', $this->buildArray($mail));

        if ($response->failed()) {
            Log::error('MailService - Sendgrid', $response->json());
            return;
        }

        $mail->message_id = $response->header('X-Message-Id');
        $mail->save();
    }

    private function buildArray(Mail $mail): array
    {
        return [
            'personalizations' => [
                [
                    'to' => [
                        [
                            'email' => $mail->to_email,
                            'name' => $mail->to_name,
                        ],
                    ],
                    'subject' => $mail->subject,
                ],
            ],
            'from' => [
                'email' => $mail->from_email,
                'name' => $mail->from_name,
            ],
            'content' => [
                [
                    'type' => 'text/plain',
                    'value' => $mail->content,
                ],
                [
                    'type' => 'text/html',
                    'value' => (new SimpleMail($mail))->render(),
                ],
            ],
            'tracking_settings' => [
                'open_tracking' => [
                    'enable' => true,
                ]
            ],
        ];
    }

    public function processMail(array $request): void
    {
        $parser = new Parser();
        $parser->setText($request['email']);
        $mail = null;
        $senderEmail = $parser->getAddresses('from')[0]['address'];

        $recipients = $parser->getAddresses('to');
        foreach ($recipients as $recipient) {
            if (! MailServiceHelper::emailBelongsToDomain($recipient['address'], $this->domain->name)) {
                continue;
            }

            $messageId = $parser->getHeader('message-id');

            if ($this->domain->mails()->where('message_id', $messageId)->exists()) {
                continue;
            }

            $replyToMail = $this->domain->mails()
                ->where('message_id', clean_message_id($parser->getHeader('in-reply-to')))
                ->first();

            $mail = new Mail([
                'message_id' => $messageId,
                'reply_to_message_id' => $parser->getHeader('in-reply-to'),
                'reply_to_mail_id' => $replyToMail->id ?? null,
                'from_name' => $parser->getAddresses('from')[0]['display'],
                'from_email' => $senderEmail,
                'to_name' => $recipient['display'],
                'to_email' => $recipient['address'],
                'subject' => $parser->getHeader('subject'),
                'content' => $parser->getMessageBody('text'),
                'raw' => $request['email'],
                'sent_at' => new Carbon($parser->getHeader('date')),
            ]);

            $address = $this->domain->addresses()
                ->where('email_lhs', MailServiceHelper::getEmailLocalName($recipient['address']))
                ->first();

            if (empty($address)) {
                $address = $this->domain->defaultAddress;
            }

            if (! empty($address)) {
                if ((new Doorman($address))->hasBlocked($senderEmail)) {
                    continue;
                }

                $mail->address()->associate($address);
                $mail->folder()->associate($address->getInboxFolder());
                $this->domain->mails()->save($mail);
            }

            if (! empty($replyToMail)) {
                $replyToMail->markOriginalMailAsUnread();
            }

            dispatch(new DiscordNotificationJob($mail));
        }
    }

    public function processEvent(array $events): void
    {
        foreach ($events as $event) {
            $messageId = explode('.', $event['sg_message_id'])[0];
            $mail = Mail::where('message_id', $messageId)->first();

            if (empty($mail)) {
                continue;
            }

            $mail->status = $event['event'];

            if ($event['event'] === 'open') {
                $mail->read_at = now();
            }

            $mail->save();
        }
    }
}
