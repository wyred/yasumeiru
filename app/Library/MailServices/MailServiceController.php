<?php

namespace App\Library\MailServices;

use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Http\Request;

class MailServiceController extends Controller
{
    public function incoming(Request $request, $domainIdentifier)
    {
        (new MailServiceManager())
            ->processIncoming(
                Team::where('identifier', $domainIdentifier)->firstOrFail(),
                $request->all()
            );
    }

    public function events(Request $request, $domainIdentifier)
    {
        (new MailServiceManager())
            ->processEvent(
                Team::where('identifier', $domainIdentifier)->firstOrFail(),
                $request->all()
            );
    }
}
