<?php

namespace App\Library\MailServices\Local;

use App\Library\MailServices\MailServiceContract;
use App\Mail\SimpleMail;
use App\Models\Mail;
use App\Models\Team;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail as Mailer;

class Local implements MailServiceContract
{
    private $domain;

    public function __construct(Team $domain)
    {
        $this->domain = $domain;
    }

    public static function getName(): string
    {
        return 'Local';
    }

    public function installWebhooks(): void
    {
        Log::info('Local Mailservice Webhook Installed for ' . $this->domain->name);
    }

    public function sendMail(Mail $mail): void
    {
        Mailer::to([
            [
                'email' => $mail->to_email,
                'name' => $mail->to_name,
            ],
        ])->send(new SimpleMail($mail));

        Log::info('Local Mailservice Sent for ' . $this->domain->name, [
            'to' => $mail->to_email,
            'from' => $mail->from_email,
            'subject' => $mail->subject,
            'content' => $mail->content,
        ]);
    }

    public function processMail(array $request): void
    {
        Log::info('Local Mailservice Mail Processed for ' . $this->domain->name, $request);
    }

    public function processEvent(array $request): void
    {
        Log::info('Local Mailservice Event Processed for ' . $this->domain->name, $request);
    }
}
