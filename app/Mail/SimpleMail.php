<?php

namespace App\Mail;

use App\Models\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SimpleMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Mail
     */
    public $mail;

    /**
     * Create a new message instance.
     *
     * @param Mail $mail
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->withSwiftMessage(function ($swiftmessage) {
            $this->mail->message_id = $swiftmessage->getId();
            $this->mail->save();
        });

        return $this->from($this->mail->from_email, $this->mail->from_name)
            ->subject($this->mail->subject)
            ->markdown('mail.simple');
    }
}
