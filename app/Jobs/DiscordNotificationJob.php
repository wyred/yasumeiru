<?php

namespace App\Jobs;

use App\Library\Discord;
use App\Models\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DiscordNotificationJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $mail;

    /**
     * Create a new job instance.
     *
     * @param Mail $mail
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domain = $this->mail->domain;

        if (empty($domain->discord_notification_url)) {
            return;
        }

        $address = $this->mail->address;
        $addressId = 'wildcard';
        if (! empty($address)) {
            $addressId = $address->id;
        }

        $embeds = [
            'fields' => [
                [
                    'name' => 'From',
                    'value' => $this->mail->from_email,
                    'inline' => true,
                ],
                [
                    'name' => 'To',
                    'value' => $this->mail->to_email,
                    'inline' => true,
                ],
                [
                    'name' => 'Subject',
                    'value' => $this->mail->subject,
                ],
                [
                    'name' => 'Content',
                    'value' => $this->mail->content,
                ],
                [
                    'name' => 'View on web',
                    'value' => route('mail', [
                        'address_id' => $addressId,
                        'mail_id' => $this->mail->id,
                    ]),
                ],
            ]
        ];

        if (empty($this->mail->address_id)) {
            $embeds['fields'][] = [
                'name' => 'Note',
                'value' => 'This mail is not attached to any address.'
            ];
        }

        (new Discord())->setWebhook($domain->discord_notification_url)
            ->setContent('New email received')
            ->embed($embeds)
            ->send();
    }
}
