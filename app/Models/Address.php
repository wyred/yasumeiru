<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'email_lhs',
        'name',
    ];

    protected static function booted()
    {
        static::created(function (Address $address) {
            $address->setupSystemFolders();
        });
    }

    public function blockedEmailAddresses()
    {
        return $this->hasMany(BlockedEmail::class);
    }

    public function domain()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function mails()
    {
        return $this->hasMany(Mail::class);
    }

    public function folders()
    {
        return $this->hasMany(Folder::class);
    }

    public function getFullAddress()
    {
        return $this->email_lhs . '@' . $this->domain->name;
    }

    public function getInboxFolder(): Model
    {
        return $this->folders()->where('slug', 'inbox')->first();
    }

    public function getOutboxFolder(): Model
    {
        return $this->folders()->where('slug', 'outbox')->first();
    }

    public function setupSystemFolders()
    {
        $this->folders()->createMany([
            [
                'slug' => 'inbox',
                'name' => 'Inbox',
                'is_system' => true,
            ],
            [
                'slug' => 'outbox',
                'name' => 'Outbox',
                'is_system' => true,
            ],
            [
                'slug' => 'trash',
                'name' => 'Trash',
                'is_system' => true,
            ],
        ]);
    }

    public function getUnreadMailsCount()
    {
        return $this->mails()
            ->where('folder_id', '!=', $this->getOutboxFolder()->id)
            ->whereNull('read_at')
            ->count();
    }

    public function getTotalMailsCount()
    {
        return $this->mails()
            ->where('folder_id', '!=', $this->getOutboxFolder()->id)
            ->count();
    }
}
