<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'name',
        'is_system',
    ];

    public static $defaultFolders = [
        ''
    ];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function mail()
    {
        return $this->hasMany(Mail::class);
    }

    public function scopeExcludeSystem($query)
    {
        return $query->where('is_system', 0);
    }

    public function scopeMovableTo($query)
    {
        return $query->where('is_system', 0)
            ->orWhere('slug', 'inbox')
            ->orWhere('slug', 'trash')
            ->orderBy('is_system')
            ->orderBy('name');
    }
}
