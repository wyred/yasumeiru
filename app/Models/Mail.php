<?php

namespace App\Models;

use App\Library\Doorman;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Jetstream\HasTeams;

class Mail extends Model
{
    use HasFactory;
    use HasTeams;
    use SoftDeletes;

    protected $fillable = [
        'message_id',
        'reply_to_message_id',
        'reply_to_mail_id',
        'from_name',
        'from_email',
        'to_name',
        'to_email',
        'subject',
        'content',
        'raw',
        'sent_at',
        'is_read',
    ];

    protected $dates = [
        'sent_at',
    ];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function domain()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }

    public function replies()
    {
        return $this->hasMany(Mail::class, 'reply_to_mail_id')->orderBy('created_at');
    }

    public function replyTo()
    {
        return $this->belongsTo(Mail::class, 'reply_to_mail_id');
    }

    public function markAsRead()
    {
        $this->read_at = now();
        $this->save();
    }

    public function markOriginalMailAsUnread()
    {
        if (empty($this->replyTo)) {
            $this->read_at = null;
            $this->save();
            return;
        }

        $this->replyTo->markOriginalMailAsUnread();
    }

    public function isBlocked(): bool
    {
        return (new Doorman($this->address))
            ->hasBlocked($this->from_email);
    }

    public function isInInbox(): bool
    {
        if (empty($this->folder_id)) {
            return false;
        }

        $inboxFolder = $this->address->getInboxFolder();
        return $inboxFolder->id === $this->folder_id;
    }

    public function isInOutbox(): bool
    {
        if (empty($this->folder_id)) {
            return false;
        }

        $outboxFolder = $this->address->getOutboxFolder();
        return $outboxFolder->id === $this->folder_id;
    }
}
