<?php

namespace App\Actions\Fortify;

use App\Library\MailServices\MailServiceManager;
use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param array $input
     * @return \App\Models\User
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),

            'domain' => ['required', 'string', 'max:255'],
            'mailservice' => ['required', 'in:' . implode(',', (new MailServiceManager())->getAvailableServiceKeys())],
            'mailservice_api_key' => ['required', 'string', 'max:255'],
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]), function (User $user, $input) {
                $this->createTeam($user, $input);
            });
        });
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @param array $input
     * @return void
     */
    protected function createTeam(User $user, array $input)
    {
        $team = Team::forceCreate([
            'user_id' => $user->id,
            'name' => $input['domain'],
            'personal_team' => true,
            'identifier' => Str::random(40),
            'mailservice' => $input['mailservice'],
            'mailservice_api_key' => $input['mailservice_api_key'],
        ]);

        $user->ownedTeams()->save($team);

        (new MailServiceManager())->setup($team);
    }
}
