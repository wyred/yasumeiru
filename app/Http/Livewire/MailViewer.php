<?php

namespace App\Http\Livewire;

use App\Library\Doorman;
use App\Library\MailServices\MailServiceManager;
use App\Models\Mail;
use Livewire\Component;
use PhpMimeMailParser\Parser;

class MailViewer extends Component
{
    /* @var \App\Models\Mail */
    public $mail;

    public $reply;
    public $replyToMailId;
    public $replyToName;
    public $returnUrl;

    public $currentFolderName;
    public $destinationFolder;

    public function mount($mailId)
    {
        $this->mail = auth()->user()->currentTeam
            ->mails()
            ->where('id', $mailId)
            ->withTrashed()
            ->firstOrFail();

        $this->returnUrl = url()->previous();
        $this->destinationFolder = $this->mail->folder->slug;
        $this->replyToMailId = $this->mail->id;
        $this->replyToName = $this->mail->from_name;
    }

    public function render()
    {
        $this->mail->markAsRead();

        return view('livewire.mail-viewer', [
            'parser' => new Parser(),
        ]);
    }

    public function hydrate()
    {
        $this->mail->load('replies');
    }

    public function setReplyTo($id, $name)
    {
        $this->replyToMailId = $id;
        $this->replyToName = $name;
    }

    public function sendReply()
    {
        $sender = $this->mail->address;

        $replyToMail = auth()->user()->currentTeam
            ->mails()
            ->where('id', $this->replyToMailId)
            ->withTrashed()
            ->firstOrFail();

        $mail = new Mail([
            'message_id' => 'unsent',
            'reply_to_mail_id' => $this->replyToMailId,
            'from_name' => $sender->name,
            'from_email' => $sender->getFullAddress(),
            'to_name' => $replyToMail->from_name,
            'to_email' => $replyToMail->from_email,
            'subject' => 'RE: ' . $replyToMail->subject,
            'content' => $this->reply,
            'raw' => $this->reply,
            'sent_at' => now(),
        ]);

        $mail->address()->associate($sender);
        $mail->domain()->associate(auth()->user()->currentTeam);
        $mail->folder()->associate($this->mail->address->getOutboxFolder());
        $mail->save();

        (new MailServiceManager())->sendEmail($mail);

        $this->reply = '';

        $this->emit('sent');
    }

    public function moveToFolder()
    {
        if (empty($this->destinationFolder) or $this->destinationFolder === 'null') {
            $this->mail->folder_id = null;
            $this->mail->save();
            return;
        }

        $folder = $this->mail->address->folders()->where('slug', $this->destinationFolder)->firstOrFail();

        $this->mail->folder_id = $folder->id;
        $this->mail->save();
    }

    public function archive()
    {
        if (! $this->mail->isInInbox()) {
            return;
        }

        $this->mail->folder_id = null;
        $this->mail->save();

        return redirect()->to($this->returnUrl);
    }

    public function block()
    {
        (new Doorman($this->mail->address))
            ->blockEmail($this->mail->from_email);
    }

    public function unblock()
    {
        (new Doorman($this->mail->address))
            ->unblockEmail($this->mail->from_email);
    }

    public function delete()
    {
        $this->mail->delete();

        return redirect()->to($this->returnUrl);
    }

    public function restore()
    {
        $this->mail->restore();

        return redirect()->to($this->returnUrl);
    }
}
