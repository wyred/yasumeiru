<?php

namespace App\Http\Livewire;

use App\Library\MailServices\MailServiceManager;
use App\Models\Folder;
use App\Models\Mail;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Mailbox extends Component
{
    use WithPagination;

    public $address = null;
    public $folders = [];
    public $activeFolder = null;

    public $showAddFolderModal = false;
    public $newFolderName;

    public $showComposeModal = false;
    public $newEmailTo;
    public $newEmailSubject;
    public $newEmailMessage;

    public function mount($addressId, $folderSlug = null)
    {
        $address = auth()->user()->currentTeam->addresses()->findOrFail($addressId);

        $this->address = $address;
        $this->loadFolders();

        $this->activeFolder = $folderSlug;
    }

    public function render()
    {
        if ($this->activeFolder === 'trash') {
            $mails = $this->address->mails()
                ->onlyTrashed()
                ->whereNull('reply_to_mail_id')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
        } else {
            $mails = $this->address->mails()
                ->when(! empty($this->activeFolder), function ($query) {
                    $folder = Folder::where('address_id', $this->address->id)
                        ->where('slug', $this->activeFolder)
                        ->firstOrFail();

                    return $query->where('folder_id', $folder->id);
                })
                ->whereNull('reply_to_mail_id')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
        }

        return view('livewire.mailbox', [
            'mails' => $mails,
            'folders' => $this->folders,
        ]);
    }

    public function loadFolders()
    {
        $this->folders = $this->address->folders()
            ->orderBy('is_system', 'DESC')
            ->orderBy('name', 'ASC')
            ->get();
    }

    public function createFolder()
    {
        $folder = new Folder([
            'slug' => Str::slug($this->newFolderName),
            'name' => $this->newFolderName,
            'is_system' => false,
        ]);
        $folder->address()->associate($this->address);
        $folder->save();

        $this->loadFolders();

        $this->newFolderName = '';
        $this->showAddFolderModal = false;
    }

    public function sendEmail()
    {
        $mail = new Mail([
            'message_id' => 'unsent',
            'from_name' => $this->address->name,
            'from_email' => $this->address->getFullAddress(),
            'to_name' => $this->newEmailTo,
            'to_email' => $this->newEmailTo,
            'subject' => $this->newEmailSubject,
            'content' => $this->newEmailMessage,
            'raw' => $this->newEmailMessage,
            'sent_at' => now(),
        ]);

        $mail->address()->associate($this->address);
        $mail->domain()->associate(auth()->user()->currentTeam);
        $mail->folder()->associate($this->address->getOutboxFolder());
        $mail->save();

        (new MailServiceManager())->sendEmail($mail);

        $this->newEmailMessage = $this->newEmailTo = $this->newEmailSubject = '';
        $this->showComposeModal = false;
    }

    public function emptyTrash()
    {
        $this->address->mails()
            ->onlyTrashed()
            ->forceDelete();
    }
}
