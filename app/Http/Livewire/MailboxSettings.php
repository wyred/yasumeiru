<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class MailboxSettings extends Component
{
    public $address;
    public $emptyTrashAfterDays;
    public $emptyTrashAfterDaysOptions = [3, 7, 14, 30, 60, 90, 365];

    public $deleteFolderSlug;
    public $moveFolderSlug;

    public $confirmingAddressDeletion = false;

    public function mount($addressId)
    {
        $this->address = auth()->user()->currentTeam->addresses()->where('id', $addressId)->firstOrFail();
        $this->emptyTrashAfterDays = $this->address->empty_trash_after_days;

        $this->deleteFolderSlug = $this->address->folders()->excludeSystem()->first()->slug ?? null;
        $this->moveFolderSlug = $this->address->folders()->movableTo()->first()->slug;
    }

    public function render()
    {
        return view('livewire.mailbox-settings');
    }

    public function save()
    {
        $this->address->empty_trash_after_days = $this->emptyTrashAfterDays;
        $this->address->save();

        $this->emit('saved');
    }

    public function rules()
    {
        return [
            'emptyTrashAfterDays' => ['required', 'in:' . implode(',', $this->emptyTrashAfterDaysOptions)]
        ];
    }

    public function saveFolders()
    {
        $this->resetErrorBag();

        Validator::make([
            'deleteFolderSlug' => $this->deleteFolderSlug,
            'moveFolderSlug' => $this->moveFolderSlug,
        ], [
            'deleteFolderSlug' => ['required', 'string', 'max:255'],
            'moveFolderSlug' => ['required', 'string', 'max:255', 'different:deleteFolderSlug'],
        ], [], [
            'deleteFolderSlug' => 'Folder to delete',
            'moveFolderSlug' => 'Folder to move emails into',
        ])->validate();

        $deleteFolderId = $this->address->folders()
            ->excludeSystem()
            ->where('slug', $this->deleteFolderSlug)
            ->pluck('id')
            ->first();

        $moveFolderId = $this->address->folders()
            ->movableTo()
            ->where('slug', $this->moveFolderSlug)
            ->pluck('id')
            ->first();

        $this->address->mails()->where('folder_id', $deleteFolderId)
            ->update(['folder_id' => $moveFolderId]);

        $this->address->folders()
            ->excludeSystem()
            ->where('slug', $this->deleteFolderSlug)
            ->delete();

        $this->emit('savedFolder');
    }

    public function confirmAddressDeletion()
    {
        $this->confirmingAddressDeletion = true;
    }

    public function deleteAddress()
    {
        \DB::transaction(function () {
            $this->address->mails()->forceDelete();
            $this->address->delete();
        });

        return redirect()->route('addresses');
    }
}
