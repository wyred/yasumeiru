<?php

namespace App\Http\Livewire;

use App\Library\MailServices\MailServiceManager;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class DomainSettings extends Component
{
    public $domain;
    public $state;

    protected $validationAttributes = [
        'state.discord_notification_url' => 'Discord Notification URL',
        'state.mailservice' => 'Mail Service',
        'state.mailservice_api_key' => 'Mail Service API Key',
        'state.catch_all_address_id' => 'Catch All Email',
    ];

    public function mount()
    {
        $this->domain = auth()->user()->currentTeam;
        $this->state = $this->domain->toArray();
    }

    public function render()
    {
        return view('livewire.domain-settings');
    }

    public function save()
    {
        $this->resetErrorBag();

        Gate::forUser(auth()->user())->authorize('update', $this->domain);

        $this->validate();

        $oldMailservice = $this->domain->mailservice;
        $oldMailserviceApiKey = $this->domain->mailservice_api_key;

        $this->domain->forceFill([
            'discord_notification_url' => $this->state['discord_notification_url'],
            'mailservice' => $this->state['mailservice'],
            'mailservice_api_key' => $this->state['mailservice_api_key'],
            'catch_all_address_id' => $this->state['catch_all_address_id'],
        ])->save();

        $this->emit('saved');

        if (
            $oldMailservice !== $this->state['mailservice']
            and $oldMailserviceApiKey !== $this->state['mailservice_api_key']
        ) {
            (new MailServiceManager())->setup($this->domain);
        }
    }

    public function rules(): array
    {
        $mailserviceManager = new MailServiceManager();

        return [
            'state.discord_notification_url' => ['nullable', 'url', 'max:255'],
            'state.mailservice' => ['required', 'in:' . implode(',', $mailserviceManager->getAvailableServiceKeys())],
            'state.mailservice_api_key' => ['nullable', 'string', 'max:255'],
            'state.catch_all_address_id' => [
                'nullable',
                'in:' . implode(',', auth()->user()->currentTeam->addresses->pluck('id')->toArray())
            ],
        ];
    }
}
