<?php

namespace App\Http\Livewire;

use App\Models\Address;
use App\Models\Team;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class AddressManager extends Component
{
    public $showAddAddressModal = false;
    public $emailAddress;
    public $emailName;

    public function render()
    {
        $domain = auth()->user()->currentTeam;

        return view('livewire.address-manager', [
            'addresses' => $domain->addresses()
                ->orderBy('email_lhs')
                ->get(),
            'domain' => $domain,
        ]);
    }

    public function createAddress()
    {
        Validator::make([
            'emailAddress' => $this->emailAddress,
            'emailName' => $this->emailName,
        ], [
            'emailAddress' => ['required', 'string', 'max:255'],
            'emailName' => ['nullable', 'string', 'max:255'],
        ])->validate();

        $domain = Team::findOrFail(auth()->user()->current_team_id);
        $address = new Address([
            'email_lhs' => strtolower($this->emailAddress),
            'name' => $this->emailName,
        ]);
        $address->domain()->associate($domain);
        $address->save();

        return $this->redirect(route('addresses'));
    }
}
