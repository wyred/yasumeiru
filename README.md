# yasumeiru

an email web application that uses third party services like sendgrid to send and receive email.

It is based on Laravel with Jetstream, TailwindCSS, Livewire and AlpineJS.

It uses Jetstream's Teams for each domain you want to set up. Having access to a team will give you access to all the
email addresses created in that team.

__It is NOT RECOMMENDED to use yasumeiru in an environment where email reliability is critical__

## Setup

1. Install it like any other laravel app
2. Enter the domain name as the team name
3. Update domain DNS records based on instructions from sendgrid
4. Obtain API key from service and store it in domain settings

## Composer packages

1. https://github.com/php-mime-mail-parser/php-mime-mail-parser

## Installation Notes

mailparse extension will be required and can be installed using `pecl install mailparse`

It must be loading after the mbstring extension:
https://stackoverflow.com/questions/21127052/php-mailparse-so-error-undefined-symbol-mbfl-convert-filter-flush

## Roadmap
- Attachments
- WYSIWYG markdown editor - if there's a good one
- Inline Attachments - not possible, but I'm open to suggestions
- No plans to integrate with other email services at the moment.

## Bugs

Feel free to open an issue.

## Feature/Enhancement Requests

Unless it requires very little effort, feature and enhancement requests will only be accepted if you are willing to pay
for my time.
