<?php

namespace Database\Seeders;

use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@yasumeiru.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('secret');
        $user->current_team_id = 1;
        $user->save();

        $team = new Team();
        $team->user_id = 1;
        $team->name = 'yasumeiru.com';
        $team->personal_team = true;
        $team->identifier = 'AHardcodedIdentifierMeantForTestingOnly1';
        $team->mailservice = 'local';
        $team->mailservice_api_key = 'InvalidApiKey';
        $team->save();
    }
}
