<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Folder;
use App\Models\Mail;
use App\Models\Team;
use Illuminate\Database\Seeder;

class MailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::factory()
            ->state([
                'user_id' => 1,
                'name' => 'badreq.com',
                'personal_team' => false,
                'identifier' => 'AHardcodedIdentifierMeantForTestingOnly2',
                'mailservice' => 'local',
            ])
            ->has(
                Address::factory()
                    ->has(
                        Folder::factory()
                            ->has(
                                Mail::factory()
                                    ->count(20)
                            )
                            ->count(2)
                    )
                    ->count(3)
            )
            ->create();

        Team::factory()
            ->state([
                'user_id' => 1,
            ])
            ->has(
                Address::factory()
                    ->has(
                        Folder::factory()
                            ->has(
                                Mail::factory()
                                    ->count(20)
                            )
                            ->count(2)
                    )
                    ->count(3)
            )
            ->create();

        Mail::factory()
            ->state([
                'team_id' => 2,
                'address_id' => 3,
                'folder_id' => 7,
            ])->count(10)
            ->create();

        Mail::where('team_id', 2)
            ->where('address_id', 3)
            ->where('folder_id', 7)
            ->get()
            ->each(function ($mail) {
                Mail::factory()
                    ->state([
                        'team_id' => 2,
                        'address_id' => 3,
                        'folder_id' => 7,
                        'reply_to_mail_id' => $mail->id,
                    ])->count(3)
                    ->create();
            });
    }
}
