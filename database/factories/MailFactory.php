<?php

namespace Database\Factories;

use App\Models\Folder;
use App\Models\Mail;
use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class MailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sentAt = $this->faker->dateTimeThisMonth;

        return [
            'team_id' => function (array $attributes) {
                $folder_id = $attributes['folder_id'];
                $folder = Folder::find($folder_id);
                return $folder->address->team_id;
            },
            'address_id' => function (array $attributes) {
                $folder_id = $attributes['folder_id'];
                $folder = Folder::find($folder_id);
                return $folder->address_id;
            },
            'message_id' => $this->faker->uuid,
            'from_name' => $this->faker->name,
            'from_email' => $this->faker->email,
            'to_name' => $this->faker->name,
            'to_email' => $this->faker->email,
            'subject' => $this->faker->catchPhrase,
            'content' => $this->faker->realText(),
            'sent_at' => $sentAt,
            'read_at' => $this->faker->boolean() ? $this->faker->dateTime : null,
            'raw' => $this->faker->text,
            'created_at' => $sentAt,
        ];
    }
}
