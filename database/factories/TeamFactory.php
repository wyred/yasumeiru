<?php

namespace Database\Factories;

use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Team::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'name' => $this->faker->domainName,
            'personal_team' => 0,
            'identifier' => Str::random(40),
            'mailservice' => 'local',
            'mailservice_api_key' => 'InvalidApiKey_' . Str::random(26),
        ];
    }
}
