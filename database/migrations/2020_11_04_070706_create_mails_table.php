<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id');
            $table->foreignId('address_id');
            $table->foreignId('folder_id')->nullable();

            $table->string('message_id')->nullable();
            $table->string('reply_to_message_id')->nullable();
            $table->unsignedBigInteger('reply_to_mail_id')->nullable();

            $table->string('from_name');
            $table->string('from_email');

            $table->string('to_name');
            $table->string('to_email');

            $table->string('subject')->nullable();
            $table->text('content')->nullable();
            $table->mediumText('raw');

            $table->dateTime('sent_at');
            $table->dateTime('read_at')->nullable();
            $table->string('status')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->index(['address_id', 'folder_id']);
            $table->index('message_id');
            $table->index('reply_to_mail_id');

            $table->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->cascadeOnDelete();

            $table->foreign('address_id')
                ->references('id')
                ->on('addresses')
                ->cascadeOnDelete();

            $table->foreign('folder_id')
                ->references('id')
                ->on('folders')
                ->nullOnDelete();

            $table->foreign('reply_to_mail_id')
                ->references('id')
                ->on('mails')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mails');
    }
}
